import axios from 'axios'

export const getGalleryListRequest = (page = 1) => async (dispatch) => {
  dispatch(_requestingGallery())
  let payload
  try {
    payload = await _getGallery(page)
  } catch (e) {
    return dispatch(_failedGetGallery(e))
  }
  return dispatch(_successGetGallery(payload))
}

export const _getGallery = async (page) => {
  return new Promise(async (resolve, reject) => {
    let payload
    try {
      payload = await axios.get(
        `https://api.500px.com/v1/photos?consumer_key=pGsYt1xmWqPDm0DAc7LGOYNTcFQRybToJtlzrATN&page=${page}`
      )
    } catch (e) {
      return reject(e)
    }
    return resolve(payload.data.photos)
  })
}

const _requestingGallery = () => ({
  type: 'REQUESTING_GELLERY'
})

const _successGetGallery = (payload) => ({
  type: 'SUCCESS_GET_GALLERY',
  payload
})

const _failedGetGallery = (errorMessage) => ({
  type: 'FAILED_GET_GALLERY',
  errorMessage
})
